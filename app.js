document.addEventListener('contextmenu', event => event.preventDefault());
let table;
let playing = false;
let dead = false;
let size = 0;
let mines = 0;
let bomb = 'x';
let empty = ' ';
let siz = 30;
createTable();
$('#diffs').on("change", createTable);

function createTable() {
    dead = false;
    table = [];
    $('#board').html("");
    switch ($("#diffs").val()) {
        case "Beginner":
            size = 8;
            mines = 10;
            break;
        case "Intermediate":
            size = 16;
            mines = 40;
            break;
        case "Expert":
            size = 30;
            mines = 99;
            break;
        case "God":
            size = 50;
            mines = 500;
            break;
        default:
            size = 8;
            mines = 10;
            break;
    }
    for (let i = 0; i < size; i++) {
        let row = []
        $('#board').append('<div class="row"></div>');
        for (let j = 0; j < size; j++) {
            row.push(empty);
            $('#board').children(".row").eq(i).append('<div class="cell"></div>');
        }
        table.push(row);
    }
    $('.cell').css({
        "height": siz + "px",
        "width": siz + "px",
        "line-height": siz + "px",
        "font-size": "0px",
        "background-size": "0%"
    });
    console.log(table);
    playing = false;
    console.log(playing);
    click();
}

function click() {
    $('.cell').mousedown(function (e) {
        if (!dead) {
            if (!playing) {
                playing = true;
                startMines($(this).parent().index(), $(this).index());
            }
            let c = $("#board").children(".row").eq($(this).parent().index()).children("div").eq($(this).index());
            switch (e.which) {
                case 1:
                    if (!c.hasClass("flag")) {
                        uncover($(this).parent().index(), $(this).index());s
                    }
                    c.css({
                        "height": siz + "px",
                        "width": siz + "px",
                        "line-height": siz + "px",
                        "background-size": "60%",
                        "background-image": "none"
                    });
                    c.removeClass("flag");
                    break;
                case 3:
                    c.css({
                        "height": siz + "px",
                        "width": siz + "px",
                        "line-height": siz + "px",
                        "background-size": "60%",
                        "background-image": "url('img/flag.png')"
                    });
                    c.addClass("flag");
                    break;
                default:
                    break;
            }
        }
    });
}

function startMines(srow, scol) {
    for (let i = 0; i < mines; i++) {
        let y = Math.floor(Math.random() * size);
        let x = Math.floor(Math.random() * size);
        if (table[y][x] === bomb || (y === srow && x === scol)) {
            i--;
        } else {
            table[y][x] = bomb;
            $('#board').children(".row").eq(y).children(".cell").eq(x).addClass("bomb");
        }
    }
    let cont;
    for (let i = 0; i < size; i++) {
        for (let j = 0; j < size; j++) {
            if (table[i][j] !== bomb) {
                cont = 0;
                for (let i2 = i - 1; i2 <= i + 1; i2++) {
                    for (let j2 = j - 1; j2 <= j + 1; j2++) {
                        if (i2 >= 0 && i2 < size && j2 >= 0 && j2 < size) {
                            if (table[i2][j2] === bomb) {
                                cont++;
                            }
                        }
                    }
                }
                if (cont > 0) {
                    table[i][j] = cont;
                    let c = $('#board').children(".row").eq(i).children(".cell").eq(j);
                    c.html(cont);
                    switch (cont) {
                        case 1:
                            c.css("color", "blue");
                            break;
                        case 2:
                            c.css("color", "green");
                            break;
                        case 3:
                            c.css("color", "red");
                            break;
                        case 4:
                            c.css("color", "dark blue");
                            break;
                        case 5:
                            c.css("color", "brown");
                            break;
                        case 6:
                            c.css("color", "cyan");
                            break;
                        case 7:
                            c.css("color", "black");
                            break;
                        case 8:
                            c.css("color", "grey");
                            break;
                        default:
                            break;
                    }
                }
                console.log(cont);
            }
        }
    }
    click();
}

function uncover(srow, scol) {
    let c = $("#board").children(".row").eq(srow).children("div").eq(scol);
    if (!c.hasClass("flag")) {
        if (table[srow][scol] === bomb) {
            dead = true;
        }
        c.css({
            "height": siz + "px",
            "width": siz + "px",
            "line-height": siz + "px",
            "font-size": siz / 2 + "px",
            "background-size": "60%",
            "background-image": "none"
        });
        if (table[srow][scol] === empty && c.hasClass("cell")) {
            c.removeClass("cell").addClass("checked");
            for (let i2 = srow - 1; i2 <= srow + 1; i2++) {
                for (let j2 = scol - 1; j2 <= scol + 1; j2++) {
                    if (i2 >= 0 && i2 < size && j2 >= 0 && j2 < size) {
                        uncover(i2, j2);
                    }
                }
            }
        } else if (table[srow][scol] === bomb) {
            playing = false;
            console.log("lost");
            $('.bomb').css({
                "height": siz + "px",
                "width": siz + "px",
                "line-height": siz + "px",
                "font-size": siz / 2 + "px",
                "background-size": "60%",
                "background-image": "url('img/bomb.png')"
            });
            c.css({
                "height": siz + "px",
                "width": siz + "px",
                "line-height": siz + "px",
                "font-size": siz / 2 + "px",
                "background-size": "60%",
                "background-image": "url('img/explosion.png')"
            });
        }
        c.removeClass("cell").addClass("checked");
    }
}